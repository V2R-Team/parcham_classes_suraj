<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wscubetech
 */?>

<?php echo get_header(); ?>
<div class="container">
	<div class="row cards-content">
		<div class="col-md-9 col-xl-9 right-sidebar">
		<div class="row">
				<?php if ( have_posts() ) : ?>
				<?php
			while ( have_posts() ) :
				the_post(); ?>

				<div class="col-md-6 col-lg-4">
					<?php
				get_template_part( 'template-parts/content', get_post_type() );
				?>
				</div>
				<?php
			endwhile;
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
				<?php echo do_shortcode('[ajax_load_more  posts_per_page="3" destroy_after="3"  transition="fade" images_loaded="true" label="Load More"] ');  ?>
			</div>
		</div>
		<div class="col-md-3 col-xl-3">
			<div class="category-box">
				<?php get_sidebar(); ?>
			</div>

		</div>
	</div>
	<?php echo get_footer(); ?>