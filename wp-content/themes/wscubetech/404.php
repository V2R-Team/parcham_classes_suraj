<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package wscubetech
 */

get_header();
the_post();
?>
<div class="container">
  <div class="row cards-content ">
    	  <div class="col-md-12 py-5 text-center">
    	        <?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'wscubetech' ); ?>
    	    <div>
    	        <img src="<?php echo get_template_directory_uri(); ?>/img/404.png" />
    	    </div>
    	  </div>
	</div>
</div>


<?php
get_footer();
?>
