<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wscubetech
 */
$url = wp_get_attachment_url( get_post_thumbnail_id($post_id) );

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<a href="<?php the_field('enter_youtube_link'); ?>" target="_blank" title="<?php echo the_title(); ?>">
	<div class="entry-content card">
		
		<img src="<?php echo $url ?>" alt="" class="card-img-top img-fluid">
		<div class="card-body">
		   <h5 class="card-title"><?php echo the_title() ?></h5>
		</div>
	
	</div>
</a>
</article>

