<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wscubetech
 */
$url = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php //the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		
		<?php endif; ?>
	</header><!-- .entry-header -->



	<a href="<?php the_field('enter_youtube_link'); ?>" target="_blank" title="<?php echo the_title(); ?>">
	<div class="entry-content card">
		<img src="<?php echo $url ?>" alt="" class="card-img-top img-fluid">
		<div class="card-body">
		   <h5 class="card-title"><?php echo the_title(); ?></h5>
		</div>
	</div>
</a>


</article><!-- #post-<?php the_ID(); ?> -->
