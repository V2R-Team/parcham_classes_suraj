<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package wscubetech
 */

get_header();
?>

<div class="container">
	<div class="row cards-content">
		<div class="col-lg-9 right-sidebar">
			<div class="row">
				<?php if ( have_posts() ) : ?>
				<div class="col-xl-12">
					<header class="page-header">
						<h1 class="page-title">
							<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for: %s', 'wscubetech' ), '<span>' . get_search_query() . '</span>' );
					?>
						</h1>
					</header><!-- .page-header -->
				</div>
				<?php
					/* Start the Loop */
					while ( have_posts() ) :
						the_post();
				?>

				<div class="col-md-6 col-lg-4">
					<?php	get_template_part( 'template-parts/content', 'search' );?>
				</div>
				<?php 
				
					endwhile;

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>
			</div>

		</div>

		<div class="col-lg-3">
			<div class="category-box">
				<?php get_sidebar(); ?>
			</div>

		</div>
	</div>
</div>

<?php

get_footer();
?>