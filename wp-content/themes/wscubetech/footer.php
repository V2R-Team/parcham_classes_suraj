<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wscubetech
 */

?>
</div><!-- #page -->
<footer id="colophon" class="site-footer">
	<!-- Start Sub Footer  -->
	<div class="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h4 class="sub-heading">Rakesh Yadav class Notes</h4>
				</div>
				<div class="col-6 col-lg-3">
					<ul class="quick-links pl-0">
						<li class="">
							<a href="#" title="Bank PO"> Bank PO </a>
						</li>
						<li class="">
							<a href="#" title="Bank SO"> Bank SO </a>
						</li>
						<li class="">
							<a href="#" title="Bank Clerk"> Bank Clerk </a>
						</li>
						<li>
							<a href="#" title="Railways RRB">Railways RRB</a>
						</li>
						<li>
							<a href="#" title="Insurance">Insurance</a>
						</li>
						<li>
							<a href="#" title="ECIL">ECIL</a>
						</li>
					</ul>
				</div>
				<div class="col-6 col-lg-3">
					<ul class="quick-links pl-0">
						<li class="">
							<a href="#" title="PSPCL"> PSPCL </a>
						</li>
						<li class="">
							<a href="#" title="ISRO"> ISRO </a>
						</li>
						<li class="">
							<a href="#" title="NABARD"> NABARD </a>
						</li>
						<li>
							<a href="#" title="TET">TET</a>
						</li>
						<li>
							<a href="#" title="UPPRPB">UPPRPB</a>
						</li>
						<li>
							<a href="#" title="Defence">Defence</a>
						</li>
					</ul>
				</div>
				<div class="col-6 col-lg-3">
					<ul class="quick-links pl-0">
						<li class="">
							<a href="#" title="AAI"> AAI </a>
						</li>
						<li class="">
							<a href="#" title="APPSC"> APPSC </a>
						</li>
						<li class="">
							<a href="#" title="Aptitude"> Aptitude </a>
						</li>
						<li>
							<a href="#" title="GK & Current Affairs">GK & Current Affairs</a>
						</li>
						<li>
							<a href="#" title="BSNL TTA">BSNL TTA</a>
						</li>
						<li>
							<a href="#" title="Delhi Police">Delhi Police</a>
						</li>
					</ul>
				</div>
				<div class="col-6 col-lg-3">
					<ul class="quick-links pl-0">
						<li class="">
							<a href="#" title="GATE"> GATE </a>
						</li>
						<li class="">
							<a href="#" title="SSC JE"> SSC JE </a>
						</li>
						<li class="">
							<a href="#" title="CIL"> CIL </a>
						</li>
						<li>
							<a href="#" title="GK & Current Affairs">GK & Current Affairs</a>
						</li>
						<li>
							<a href="#" title="BSNL TTA">BSNL TTA</a>
						</li>
						<li>
							<a href="#" title="Delhi Police">Delhi Police</a>
						</li>
					</ul>
				</div>
				<div class="col-lg-12">
					<h4 class="sub-heading">Pradeep Yadav class Notes</h4>
				</div>
				<div class="col-6 col-lg-3">
					<ul class="quick-links pl-0">
						<li class="">
							<a href="#" title="Bank PO"> Bank PO </a>
						</li>
						<li class="">
							<a href="#" title="Bank SO"> Bank SO </a>
						</li>
						<li class="">
							<a href="#" title="Bank Clerk"> Bank Clerk </a>
						</li>
						<li>
							<a href="#" title="Railways RRB">Railways RRB</a>
						</li>
						<li>
							<a href="#" title="Insurance">Insurance</a>
						</li>
						<li>
							<a href="#" title="ECIL">ECIL</a>
						</li>
					</ul>
				</div>
				<div class="col-6 col-lg-3">
					<ul class="quick-links pl-0">
						<li class="">
							<a href="#" title="PSPCL"> PSPCL </a>
						</li>
						<li class="">
							<a href="#" title="ISRO"> ISRO </a>
						</li>
						<li class="">
							<a href="#" title="NABARD"> NABARD </a>
						</li>
						<li>
							<a href="#" title="TET">TET</a>
						</li>
						<li>
							<a href="#" title="UPPRPB">UPPRPB</a>
						</li>
						<li>
							<a href="#" title="Defence">Defence</a>
						</li>
					</ul>
				</div>
				<div class="col-6 col-lg-3">
					<ul class="quick-links pl-0">
						<li class="">
							<a href="#" title="AAI"> AAI </a>
						</li>
						<li class="">
							<a href="#" title="APPSC"> APPSC </a>
						</li>
						<li class="">
							<a href="#" title="Aptitude"> Aptitude </a>
						</li>
						<li>
							<a href="#" title="GK & Current Affairs">GK & Current Affairs</a>
						</li>
						<li>
							<a href="#" title="BSNL TTA">BSNL TTA</a>
						</li>
						<li>
							<a href="#" title="Delhi Police">Delhi Police</a>
						</li>
					</ul>
				</div>
				<div class="col-6 col-lg-3">
					<ul class="quick-links pl-0">
						<li class="">
							<a href="#" title="GATE"> GATE </a>
						</li>
						<li class="">
							<a href="#" title="SSC JE"> SSC JE </a>
						</li>
						<li class="">
							<a href="#" title="CIL"> CIL </a>
						</li>
						<li>
							<a href="#" title="GK & Current Affairs">GK & Current Affairs</a>
						</li>
						<li>
							<a href="#" title="BSNL TTA">BSNL TTA</a>
						</li>
						<li>
							<a href="#" title="Delhi Police">Delhi Police</a>
						</li>
					</ul>
				</div>
				<div class="col-lg-12">
					<h4 class="sub-heading">Sandeep Yadav class Notes</h4>
				</div>
				<div class="col-6 col-lg-3">
					<ul class="quick-links pl-0">
						<li class="">
							<a href="#" title="Bank PO"> Bank PO </a>
						</li>
						<li class="">
							<a href="#" title="Bank SO"> Bank SO </a>
						</li>
						<li class="">
							<a href="#" title="Bank Clerk"> Bank Clerk </a>
						</li>
						<li>
							<a href="#" title="Railways RRB">Railways RRB</a>
						</li>
						<li>
							<a href="#" title="Insurance">Insurance</a>
						</li>
						<li>
							<a href="#" title="ECIL">ECIL</a>
						</li>
					</ul>
				</div>
				<div class="col-6 col-lg-3">
					<ul class="quick-links pl-0">
						<li class="">
							<a href="#" title="PSPCL"> PSPCL </a>
						</li>
						<li class="">
							<a href="#" title="ISRO"> ISRO </a>
						</li>
						<li class="">
							<a href="#" title="NABARD"> NABARD </a>
						</li>
						<li>
							<a href="#" title="TET">TET</a>
						</li>
						<li>
							<a href="#" title="UPPRPB">UPPRPB</a>
						</li>
						<li>
							<a href="#" title="Defence">Defence</a>
						</li>
					</ul>
				</div>
				<div class="col-6 col-lg-3">
					<ul class="quick-links pl-0">
						<li class="">
							<a href="#" title="AAI"> AAI </a>
						</li>
						<li class="">
							<a href="#" title="APPSC"> APPSC </a>
						</li>
						<li class="">
							<a href="#" title="Aptitude"> Aptitude </a>
						</li>
						<li>
							<a href="#" title="GK & Current Affairs">GK & Current Affairs</a>
						</li>
						<li>
							<a href="#" title="BSNL TTA">BSNL TTA</a>
						</li>
						<li>
							<a href="#" title="Delhi Police">Delhi Police</a>
						</li>
					</ul>
				</div>
				<div class="col-6 col-lg-3">
					<ul class="quick-links pl-0">
						<li class="">
							<a href="#" title="GATE"> GATE </a>
						</li>
						<li class="">
							<a href="#" title="SSC JE"> SSC JE </a>
						</li>
						<li class="">
							<a href="#" title="CIL"> CIL </a>
						</li>
						<li>
							<a href="#" title="GK & Current Affairs">GK & Current Affairs</a>
						</li>
						<li>
							<a href="#" title="BSNL TTA">BSNL TTA</a>
						</li>
						<li>
							<a href="#" title="Delhi Police">Delhi Police</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- End Sub Footer  -->
	<div class="footer">
		<div class="container ">
			<div class="row border-bottom footer-border">
				<div class="col-lg-6">
					<h4 class="sub-heading">Our Apps</h4>
					<ul class="apps-list pl-0">
						<li>
							<div class="app-img">
								<img src="<?php echo get_template_directory_uri(); ?>/img/android.svg" width="30" alt=""
									class="img-fluid">
							</div>
							<div class="app-content pl-3">
								<h6>Parcham Classes App</h6>
								<a href="#" title="Download Now" target="_blank">Download Now</a>
							</div>
						</li>
					</ul>
				</div>
				<div class="col-lg-6">
					<h4 class="sub-heading">Follow Us</h4>
					<ul class="social-list pl-0">
						<li><a href="#" class="" title="Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
						</li>
						<li><a href="#" class="" title="Twitter" target="_blank"><i class="fab fa-twitter"></i></a>
						</li>
						<li><a href="#" class="" title="Linkedin" target="_blank"><i class="fab fa-linkedin-in"></i></a>
						</li>
						<li><a href="#" class="" title="Instagram" target="_blank"><i class="fab fa-instagram"></i></a>
						</li>
						<li><a href="#" class="" title="Youtube" target="_blank"><i class="fab fa-youtube"></i></a>
						</li>
					</ul>
				</div>
			</div>
			<div class="row copywrite">
				<div class="col-lg-12 text-center">
					<p>Copyright &#169; 2020 Parcham Classes Pvt. Ltd.: All rights reserved</p>
				</div>
			</div>
		</div>
	</div>
</footer><!-- #colophon -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"></script>
<?php wp_footer(); ?>

</body>

</html>