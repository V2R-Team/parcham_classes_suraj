<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>

<div class="col-md-6 col-lg-4">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<a href="<?php the_field('enter_youtube_link'); ?>" target="_blank" title="<?php echo the_title(); ?>">
	<div class="entry-content card">
		<?php if (! has_post_thumbnail() ) { echo ' card-img-top img-fluid'; } ?>
   <?php if ( has_post_thumbnail() ) {
      the_post_thumbnail('medium');
   }?>
		<div class="card-body">
		   <h5 class="card-title"><?php echo the_title() ?></h5>
		</div>
	</div>
</a>
</article>
</div>